import { Selector } from "testcafe";

export default class Vsd {
  constructor() {
    this.monthViewButton = Selector(".supercalendar__btn").nth(0);
    this.headerLabel = Selector("#vsd-header-summary time .out").nth(0);
    this.weekViewSelectedDay = {
      label: Selector(
        "#weekview-train .oui-weekview__day--selected .oui-weekview__day-date"
      ).nth(0),
      price: Selector(
        "#weekview-train .oui-weekview__day--selected .oui-weekview__day-price span span"
      ).nth(0)
    };
    this.ctaButtonPriceLabel = Selector(
      "proposal-details-cta .price-template span span span"
    ).nth(0);
  }
}
