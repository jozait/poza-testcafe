import { Selector } from "testcafe";
import moment from "moment";

export default class MonthView {
  constructor() {
    this.monthViewContainer = Selector(
      ".vsc__container #calendar-widget__container"
    );
    this.monthViewDays = this.monthViewContainer.find(
      "#cdp-calendar .month .day:not(.disabled)"
    );
  }

  async getButtonXDayAfterNow(x) {
    const now = moment();
    const dayToSearch = now
      .add(x, "day")
      .date()
      .toString();
    const count = await this.monthViewDays.count;

    for (let i = 0; i < count; i++) {
      const day = this.monthViewDays.nth(i);
      const dateLabel = await day.find(".date").nth(0).textContent;

      if (dateLabel === dayToSearch) {
        return day;
      }
    }
  }
}
